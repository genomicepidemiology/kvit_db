import os, sys, urllib.request, argparse, tarfile

def request_data(url, filename=None):
    try:
        if filename:
            urllib.request.urlretrieve(url, filename) 
        else:
            response = urllib.request.urlopen(url)
    except urllib.request.HTTPError as Response_error:
        sys.exit('{} {}. <{}>\n Reason: {}'.format(Response_error.code, Response_error.msg,
                                           Response_error.geturl(), Response_error.read()))
    except urllib.error.URLError as error:
        
        sys.exit(("Error downloading url '{}'. The date_tag might not exists.\n" +
                 " URLError reason: {}").format(url, error.reason))
    if filename:
        return
    else:
        data = response.read().decode("utf-8")
    return data

# Parsing input arguments
parser = argparse.ArgumentParser()
parser.add_argument('-o','--outdir', help="Output directory, if not specified it is the" + 
                    "directory where the INSTALL.py script is located", 
                    default=os.path.dirname(sys.argv[0]))
parser.add_argument('-t', '--date_tag', help= "Date tag for the database to be downloaded",
                    default="latest")
parser.add_argument('-s', '--species', nargs="+", help="Species that should be downloaded",
                    default=[])


args = parser.parse_args()

outdir = os.path.abspath(args.outdir)
if not os.path.exists(outdir):
    sys.exit("Output directory '{}' does not exists".format(outdir))

date_tag = args.date_tag
url = "ftp://ftp.cbs.dtu.dk/public/CGE/databases/KVIT/version/{}/config".format(date_tag)
print(url)
data = request_data(url)
lines = data.split("\n")


species_list = []

for line in lines:
    if line.startswith("#") or line.strip() == "":
        continue
    else:
        species = line.split("\t")[1]
        print(species)
        species_list.append(species)

# Check that user specifed species exists
for species in args.species:
    if species not in species_list:
        sys.exit("Species '{}' does not exists in the database '{}'. The following species exists:\n{}".format(
                 species, date_tag, "\n".join(species_list)))

# Get config file
request_data(url, filename=os.path.join(outdir, "config"))

# Get database_overview file
url = "ftp://ftp.cbs.dtu.dk/public/CGE/databases/KVIT/version/{}/database_overview.txt".format(date_tag)
request_data(url, filename=os.path.join(outdir, "database_overview.txt"))

# Reset species list if user specified species
if args.species != []:
    species_list = args.species

# Download database
for species in species_list:
    species_dir = os.path.join(outdir, species)
    os.makedirs(species_dir, exist_ok=True)

    species_url = ("ftp://ftp.cbs.dtu.dk/public/CGE/databases/KVIT/version/{}/" + 
                   "{}.tar.gz").format(date_tag, species)
    species_tarfile = "{}.tar.gz".format(os.path.join(species_dir, species))
    request_data(species_url, filename=species_tarfile)
        
    tar = tarfile.open(species_tarfile)
    tar.extractall(path=outdir)
    os.remove(species_tarfile)
