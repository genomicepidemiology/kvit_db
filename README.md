KVIT DATABASES
===================


**Important note: Due to server maintenance, the ftp site for downloading the database is currently unavailable. We expect the ftp sites to be available again within two weeks.**

This project documents the installation, validation and update of KVIT databases.
The databases stored on our FTP site, are needed by the program KVIT (https://bitbucket.org/genomicepidemiology/kvit)


Documentation
=============

## Content of the repository

1. INSTALL.py      - the program for installing all databases or one specific database
2. README.md  

## Databases

* Anelloviridae
* Arenaviridae
* Arteriviridae
* Astroviridae
* Birnaviridae
* Bornaviridae
* Bunya-Hantaviridae
* Bunya-Peribunyaviridae
* Bunya-Phenuiviridae
* Bunyavirales-extra
* Caliciviridae
* Circoviridae
* Coronaviridae
* Deltavirus
* Filoviridae
* Flavivirus
* Hepacivirus
* Hepadnaviridae
* Hepeviridae
* HIV1
* Orthomyxoviridae
* Papillomaviridae
* Paramyxoviridae
* Parvoviridae
* Pegivirus
* Pestivirus
* Picobirnaviridae
* Picornavirales-extra
* Picornaviridae
* Pneumoviridae
* Polyomaviridae
* Reoviridae
* RetroviridaenotHIV1
* Rhabdoviridae
* Togaviridae
* Unclassified

## Installation - Clone repository
```bash
# Go to wanted location for KVIT db 
cd /path/to/some/dir
# Clone and enter the KVIT db directory
git clone https://bitbucket.org/genomicepidemiology/kvit_db.git
cd kvit_db
```

## Download and install KVIT database(s)

The INSTALL program allows you to install either all available databases or to download one specific database.
There is also an extra option to download database(s) from a specific date tag (update). If there is no date tag specified, database(s) will be downloaded automatically 
from the most current update. The INSTALL program downloads the binary files of the database(s).

usage: INSTALL.py [-h] [-o OUTDIR] [-t DATE_TAG] [-s SPECIES [SPECIES ...]]

optional arguments:
  -h, --help            show this help message and exit
  -o OUTDIR, --outdir OUTDIR
                        Output directory, if not specified it is the directory
                        where the INSTALL.py script is located
  -t DATE_TAG, --date_tag DATE_TAG
                        Date tag for the database to be downloaded
  -s SPECIES [SPECIES ...], --species SPECIES [SPECIES ...]
                        Species that should be downloaded

```bash
# Below we provide alternative example commands to download the database(s) you want.
#1. Install all KVIT databases from latest update
python3 INSTALL.py
#2  Install all KVIT databases from latest update to a specific path
python3 INSTALL.py -o /path/to/some/dir
#3. Install all KVIT databases from specific date - you can find available updates in our FTP site.
python3 INSTALL.py -t 20190405
#4. Install only one KVIT database from latest update, e.g. Anelloviridae
python3 INSTALL.py -s Anelloviridae
#4. Install only the Anelloviridae KVIT database from specific date - you can find available updates in our FTP site.
pyhton3 INSTALL.sh -s Anelloviridae -t 20190405
```


## Update the KVIT database(s)

The KVIT databases are updated automaticly once a week. To stay in sync with this update the install script should be run every week. The updated databases are all available on the ftp-site


## Available databases
You can find all our available databases in our [FTP site](ftp://ftp.cbs.dtu.dk/public/CGE/databases/KVIT/version/)

## Who do I talk to?
To get in contact with us, please contact cgehelp@cbs.dtu.dk
